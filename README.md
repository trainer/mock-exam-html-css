# Mock exam

## Please read the instructions carefully

1. Comments are in a yellow sticky paper.
2. Please note the colors carefully. Match the font and background colors to the best of your ability.
3. Font sizes are also important. Match the image as much as possible.
4. Determine heading levels (h1, h2, h3, h4) based on the content and its location.

Best of luck!

---

![Mockups](https://gitlab.com/abijeet.p/mock-exam-html-css/raw/master/batch9_mockup.png)

